var d = require("dotenv").config();
var m = require("mustache");
var exec = require("child_process").exec;
const execSync = require("child_process").execSync;
var fs = require("fs");
var path = require("path");
var jsforce = require("jsforce");

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
////////////              Project variables            //////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
const bitbucketRepo_starter = "git@bitbucket.org:liveoakbank/starter.git";
const bitbucketRepo_starter_templates =
  "git@bitbucket.org:liveoakbank/starter-templates.git";
var cwd = process.cwd();

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
////////////                Apex commands              //////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

var START_BANKING_SERVICES_APEX_CMD =
  "System.debug('INFO[starter-installer]: Installer kicks off banking services........');" +
  "LiveOak.BankingServicesDataProvider provider = new LiveOak.BankingServicesDataProvider();" +
  "provider.run(null);" +
  "nForce.LifeCycleDao dao = new nForce.LifeCycleDao(provider);" +
  "dao.performAllDml();";

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
////////////              Support functions            //////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

function usage() {
  console.log("Usage: node installer.js <help|upload|build> <options>");
  console.log("");
}

function isEmpty(val) {
  return val === undefined || val == null || val.length <= 0 ? true : false;
}

function checkEnv() {
  if (isEmpty(process.env.PROJECT_NAME)) {
    // foo could get resolved and it's defined
    console.log(
      "ERROR[installer]: Environment is not configured. Please configure your .evn file."
    );
    process.exit(1);
  }
}

function getTemplateTypeParam() {
  var templateType = process.argv[3];
  if (isEmpty(templateType)) {
    // foo could get resolved and it's defined
    console.log(
      "ERROR[installer]: Build step requires template type. Usage: node installer.js build <templateType>"
    );
    process.exit(1);
  }
  return templateType;
}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
////////////    Mustache based template normalizaiton  //////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
function parse(data, view) {
  var output = m.render(data.toString(), view);
  process.stdout.write(output);
}

function safeMkDir(dirPath) {
  var cmd = "mkdir -p " + dirPath;
  execSync(cmd);
}

function resolveTagsInFiles(sourceDir, destDir) {
  var view = {
    PROJECT_NAME: process.env.PROJECT_NAME,
    SF_USER_ID: process.env.SF_USER_ID,
    SF_PASSWORD: process.env.SF_PASSWORD,
    ORG_HOST: process.env.ORG_HOST,
    NAMESPACE: process.env.NAMESPACE,
    COMMUNITY_URL_PREFIX: process.env.COMMUNITY_URL_PREFIX,
    COMMUNITY_URL_PREFIX_UNDERSCORE:
      process.env.COMMUNITY_URL_PREFIX_UNDERSCORE,
    AWS_ACCESS_KEY: process.env.AWS_ACCESS_KEY,
    AWS_SECRET_ACCEESS_KEY: process.env.AWS_SECRET_ACCEESS_KEY,
    CI_USER_ID: process.env.CI_USER_IDl,
    CI_PASSWORD: process.env.CI_PASSWORD,
    VF_PAGES_NAMESPACE: process.env.VF_PAGES_NAMESPACE
  };

  var cmd = "ls -p " + sourceDir;
  var sourceDirList = execSync(cmd)
    .toString()
    .split("\n");
  for (i in sourceDirList) {
    // Remove hidded files
    var templateFileName = sourceDirList[i].trim();
    if (
      !(
        templateFileName.length == 0 ||
        templateFileName.startsWith(".") ||
        templateFileName.endsWith("/")
      )
    ) {
      var sourceFile = sourceDir + path.sep + templateFileName;

      if (isFileBinary(sourceFile)) {
        console.log(
          "INFO[installer]: Found binary file. Will not apply pattern matching. File =[" +
            sourceFile +
            "]"
        );
        var destFile = destDir + path.sep + templateFileName.toString();
        fs.copyFileSync(sourceFile, destFile);
      } else {
        var resolvedFileName = m.render(templateFileName.toString(), view);
        var destFile = destDir + path.sep + resolvedFileName;
        console.log(
          "INFO[installer]: Generating file from src=[" +
            sourceFile +
            "] to=[" +
            destFile +
            "]"
        );
        var templateContent = fs.readFileSync(sourceFile);
        var parsedConent = m.render(templateContent.toString(), view);
        fs.writeFileSync(destFile, parsedConent);
      }
    }
  }
}

function isFileBinary(filePath) {
  var fileContent = fs.readFileSync(filePath);
  return /\ufffd/.test(fileContent) === true;
}

function getTemplatePathPrefix(templateType) {
  return "templates" + path.sep + templateType;
}

function getTemplateSourceDir(templatePathPrefix, templateShortPath) {
  return cwd + path.sep + templatePathPrefix + templateShortPath;
}

function getRecursiveListOfDirs(templatePathPrefix) {
  console.log("TMP tmpl-dir=[" + templatePathPrefix + "]");
  var prefixLenght = templatePathPrefix.length;
  var cmd = "find " + templatePathPrefix + "/* -type d";
  var dirList = execSync(cmd)
    .toString()
    .split("\n");
  // Remove prefix
  var shortDirs = [];
  for (i in dirList) {
    var dirPath = dirList[i].trim();
    if (dirPath.length != 0) {
      shortDirs.push(dirList[i].substring(prefixLenght, dirList[i].length));
    }
  }
  // template root what might contain config files such as build.properties
  shortDirs.push("");
  return shortDirs;
}

function refreshDirectory(templateType) {
  var templatePathPrefix = getTemplatePathPrefix(templateType);
  var templateShortDirList = getRecursiveListOfDirs(templatePathPrefix);
  for (i in templateShortDirList) {
    var templateShortDirPath = templateShortDirList[i];
    var sourceDir = getTemplateSourceDir(
      templatePathPrefix,
      templateShortDirPath
    );
    var destDir = cwd + templateShortDirPath;
    safeMkDir(destDir);
    console.log(
      "INFO[generate-from-template]: src=[" +
        sourceDir +
        "] dest=[" +
        destDir +
        ""
    );
    resolveTagsInFiles(sourceDir, destDir);
  }
}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
////////////   Bitbucket upload and archive functions  //////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

function uploadFromGitByPattern(bitbucketRepo, pattern) {
  console.log(
    "INFO[installer]: Uploading  [" + pattern + "] from [" + bitbucketRepo + "]"
  );
  var cmd =
    "git archive --format=tar --remote " +
    bitbucketRepo +
    " HEAD " +
    pattern +
    " | tar xf -";
  console.log("INFO[installer]: Command  [" + cmd + "]");
  execSync(cmd);
}

function uploadTemplate(templateType) {
  console.log(
    "INFO[installer]: Running template upload from bitbucket starter at [" +
      bitbucketRepo_starter +
      "]"
  );
  uploadFromGitByPattern(
    bitbucketRepo_starter_templates,
    "templates/" + templateType
  );
}

function uploadDependency(templateType) {
  var dependencyFile =
    "dependencies/installedPackages" +
    path.sep +
    templateType +
    ".installedPackage";
  console.log(
    "INFO[installer]: Adding most recent dependency from [" +
      bitbucketRepo_starter +
      "] at [" +
      dependencyFile +
      "]"
  );
  uploadFromGitByPattern(bitbucketRepo_starter_templates, dependencyFile);
}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
////////////         jsForce remote calls to Org       //////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
function runApexOnOrg(conn, cmd) {
  console.log("INFO[runApexOnOrg] Executing remove Apex on Org:");
  console.log("INF)[runApexOnOrg] Cmd:[" + cmd + "]");

  // execute anonymous Apex Code
  conn.tooling.executeAnonymous(cmd, function(err, res) {
    if (err) {
      return console.error(err);
    }
    console.log("INFO[runApexOnOrg]: compiled?: " + res.compiled); // compiled successfully
    console.log("INFO[runApexOnOrg]: executed?: " + res.success); // executed successfully
  });
}

function startBankingServicesOnOrg(conn) {
  runApexOnOrg(conn, START_BANKING_SERVICES_APEX_CMD);
}

function generateTestDataOnOrg(conn) {
  var templateListMsg = {
    templateNames: [
      "TestDataFactoryTemplate_FAKE_CHECKING",
      "TestDataFactoryTemplate_PERSONAL_SAVINGS",
      "TestDataFactoryTemplate_PERSONAL_1Y_CD"
    ]
  };
  var requestOptions = {
    headers: {
      Version: "2.0",
      "Content-Type": "application/json",
      Accept: "application/json"
    }
  };

  var targetUrl = "/API_TDF/test-data-factory";
  console.log(
    "INFO[installer] : Runs api call url=[" + targetUrl + "] .....(ver 9)"
  );
  conn.apex.post(targetUrl, templateListMsg, requestOptions, function(
    err,
    res
  ) {
    if (err) {
      console.log("ERR=[" + JSON.stringify(err, null, 2) + "]");
      console.log("RES=[" + JSON.stringify(res, null, 2) + "]");
      return console.error(err);
    }
    console.log("response: ", res);
  });
}

function sfLoginRunLogout(fnc) {
  // Debug
  console.log(
    "INFO[sfLoginRunLogout]: process.env.ORG_HOST=[" +
      process.env.ORG_HOST +
      "];"
  );
  // console.log('INFO[sfLoginRunLogout]: process.env.SF_USER_ID=[' + process.env.SF_USER_ID + ']');
  // console.log('INFO[sfLoginRunLogout]: process.env.SF_PASSWORD=[' + process.env.SF_PASSWORD + ']');

  var conn = new jsforce.Connection({
    loginUrl: "https://login.salesforce.com"
  });
  conn.login(process.env.SF_USER_ID, process.env.SF_PASSWORD, function(
    err,
    userInfo
  ) {
    if (err) {
      return console.error("ERROR[sfLoginRunLogout]: Details=[" + err + "]");
    }
    // User have loged-in, now we run custom function.
    fnc(conn);
  });
  conn.logout();
}

function getMetadataList(conn) {
  var types = [{ type: "CustomObject", folder: null }];
  var types = [{ type: null, folder: null }];
  conn.metadata.list(types, "39.0", function(err, metadata) {
    if (err) {
      return console.error("err", err);
    }
    var meta = metadata[0];
    console.log("metadata count: " + metadata.length);
    console.log("createdById: " + meta.createdById);
    console.log("createdByName: " + meta.createdByName);
    console.log("createdDate: " + meta.createdDate);
    console.log("fileName: " + meta.fileName);
    console.log("fullName: " + meta.fullName);
    console.log("id: " + meta.id);
    console.log("lastModifiedById: " + meta.lastModifiedById);
    console.log("lastModifiedByName: " + meta.lastModifiedByName);
    console.log("lastModifiedDate: " + meta.lastModifiedDate);
    console.log("manageableState: " + meta.manageableState);
    console.log("namespacePrefix: " + meta.namespacePrefix);
    console.log("type: " + meta.type);
  });
}

function configureUser(conn) {
  // Configure User's role and knowledge
  conn.identity(function(err, identity) {
    if (err) {
      return console.error(err);
    }
    console.log("User Identity=[" + JSON.stringify(identity, null, 2) + "]");

    // Get list of available roles, find CEO and make current user and CEO
    conn.query("SELECT Id, Name FROM UserRole", function(err, userRoles) {
      if (err) {
        return console.error(err);
      }
      for (i in userRoles.records) {
        console.log(
          "Row : id=[" +
            userRoles.records[i].Id +
            "] name=[" +
            userRoles.records[i].Name +
            "]"
        );
        if (userRoles.records[i].Name === "CEO") {
          conn
            .sobject("User")
            .update(
              {
                Id: identity.user_id,
                UserRoleId: userRoles.records[i].Id,
                UserPermissionsKnowledgeUser: true
              },
              function(err, ret) {
                if (err || !ret.success) {
                  return console.error(err, ret);
                }
                console.log(
                  "User has been updated=[" + JSON.stringify(ret, null, 2) + "]"
                );
              }
            );
          return;
        }
      }
    });
  });
}

function configurePortal(conn) {
  urlPath =
    "https://" +
    process.env.COMMUNITY_URL_PREFIX +
    "." +
    process.env.ORG_HOST +
    ".salesforce.com";
  // Configure CORS whitelist entry
  //conn.sobject("CorsWhitelistEntry").create({ UrlPattern: urlPath }, function (err, ret) {
  //  if (err) { return console.error(err); }
  //  console.log("New CorsWhitelistEntry has been created =[" + JSON.stringify(ret, null, 2) + "]");
  //});

  // Add an Organization Wide Email Address.  no-reply, noreply@liveoakbank.com
  conn
    .sobject("OrgWideEmailAddress")
    .create(
      {
        Address: "noreply@liveoakbank.com",
        DisplayName: "no-reply",
        IsAllowAllProfiles: true
      },
      function(err, ret) {
        if (err) {
          return console.error(err);
        }
        console.log(
          "New OrgWideEmailAddress has been created =[" +
            JSON.stringify(ret, null, 2) +
            "]"
        );
      }
    );
}

function configureCpCommunityProfie(conn) {
  conn.query("SELECT Id, Name FROM Profile", function(err, profile) {
    if (err) {
      return console.error(err);
    }
    for (i in profile.records) {
      console.log(
        "Row : id=[" +
          profile.records[i].Id +
          "] name=[" +
          profile.records[i].Name +
          "]"
      );
      // if (profile.records[i].Name === 'CP_CommunityPlusUser') {
      if (profile.records[i].Name === "CP_CommunityPlusUser") {
        console.log(
          "Found profile=[" + JSON.stringify(profile.records[i], null, 2) + "]"
        );

        conn
          .sobject("Profile")
          .update(
            {
              Id: identity.user_id,
              UserRoleId: userRoles.records[i].Id,
              UserPermissionsKnowledgeUser: true
            },
            function(err, ret) {
              if (err || !ret.success) {
                return console.error(err, ret);
              }
              console.log(
                "Profile has been updated=[" +
                  JSON.stringify(ret, null, 2) +
                  "]"
              );
            }
          );
        return;
      }
    }
    console.log(
      "ERROR: Profile CP_CommunityPlusUser is not yet configured in your org."
    );
  });
}

function queryEmailTemplates(conn) {
  conn.query(
    "select ApiVersion, Body, BrandTemplateId, CreatedById, CreatedDate, Description, DeveloperName, Encoding, FolderId, HtmlValue, Id, IsActive, LastModifiedById, LastModifiedDate, LastUsedDate, Markup, Name, NamespacePrefix, OwnerId, RelatedEntityType, Subject, SystemModstamp, TemplateStyle, TemplateType, TimesUsed, UiType from EmailTemplate",
    function(err, tempate) {
      if (err) {
        return console.error(err);
      }
      for (i in tempate.records) {
        console.log(
          "Found profile=[" + JSON.stringify(tempate.records[i], null, 2) + "]"
        );
      }
    }
  );
}

function getMetadataDescription(conn) {
  conn.metadata.describe("44.0", function(err, metadata) {
    if (err) {
      return console.error("err", err);
    }
    for (var i = 0; i < metadata.length; i++) {
      var meta = metadata[i];
      console.log("organizationNamespace: " + meta.organizationNamespace);
      console.log("partialSaveAllowed: " + meta.partialSaveAllowed);
      console.log("testRequired: " + meta.testRequired);
      console.log("metadataObjects count: " + metadataObjects.length);
    }
  });
}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
////////////////////    Main switch functions   /////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

function build(templateType) {
  var sourceDir = cwd + path.sep + "templates" + path.sep + templateType;
  fs.stat(sourceDir, function(err) {
    if (err) {
      console.log(
        "ERROR[Build]: Invalid template or missing template. Details=[" +
          sourceDir +
          "]"
      );
      process.exit(-1);
    }
  });
  refreshDirectory(templateType);
}

function query(templateType) {
  switch (templateType) {
    case "email-templates":
      sfLoginRunLogout(queryEmailTemplates);
      break;
    default:
      console.log("ERROR[build]. Invalid template type [" + templateType + "]");
      process.exit(-1);
  }
}

function config(templateType) {
  switch (templateType) {
    case "user":
      console.log(
        "INFO[config]: Executes remote org calls to configure user ...."
      );
      sfLoginRunLogout(configureUser);
      break;
    case "services":
      console.log("INFO[config]: Starts Banking services on remote org.......");
      sfLoginRunLogout(startBankingServicesOnOrg);
      break;
    case "portal":
      console.log(
        "INFO[config]: Executes remote org calls to configure portal (e.g. CORS) ...."
      );
      sfLoginRunLogout(configurePortal);
      break;
    case "cp-community-profile":
      console.log(
        "INFO[config]: Executes remote org calls to configure profile of CP_CommunityPlusUser ...."
      );
      sfLoginRunLogout(configureCpCommunityProfie);
      break;
    case "sfdc-deposits-portal-metadata":
      console.log(
        "INFO[config]: Executes remote api call on org to generate test data....."
      );
      sfLoginRunLogout(generateTestDataOnOrg);
      break;
    default:
      console.log("ERROR[build]. Invalid template type [" + templateType + "]");
      process.exit(-1);
  }
}

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
////////////////////      Run Main script       /////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
checkEnv();
console.log("Param-1=[" + process.argv[1] + "]");
console.log("Param-2=[" + process.argv[2] + "]");
console.log("Param-3=[" + process.argv[3] + "]");
console.log("Param-4=[" + process.argv[4] + "]");

var step = process.argv[2];
console.log("CWD:[" + process.cwd() + "]");
switch (step) {
  case "dependency":
    uploadDependency(getTemplateTypeParam());
    break;
  case "upload":
    uploadTemplate(getTemplateTypeParam());
    break;
  case "build":
    build(getTemplateTypeParam());
    break;
  case "config":
    config(getTemplateTypeParam());
    break;
  case "query":
    query(getTemplateTypeParam());
    break;
  default:
    usage();
}

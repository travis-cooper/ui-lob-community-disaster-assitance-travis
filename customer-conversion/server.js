let express = require("express");
let http = require("http");
let jsforce = require("jsforce");
let request = require("request");
let parseString = require("xml2js").parseString;
let fs = require("fs");

var getCreds = function() {
  var fileContent = fs.readFileSync("./../build.properties", "utf8");
  var q = new RegExp(/sf.username\s=\s(\S+)\nsf.password\s=\s(\S+)/);
  var r = q.exec(fileContent);
  console.log("*******************");
  console.log(r);
  var username = r[1].trim();
  var password = r[2].trim();

  return { username: username, password: password };
};

let sf = getCreds();
let conn = new jsforce.Connection();

const port = process.env.PORT || "3000";
const app = express();

app.set("port", port);

app.all("/services/apexrest/*?*", function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");

  var url = conn.instanceUrl + req.url;
  var r = null;
  if (req.method === "POST") {
    r = request.post({ uri: url, json: req.body });
  } else if (req.method === "PUT") {
    r = request.put({ uri: url, json: req.body });
  } else if (req.method === "DELETE") {
    r = request.delete({ uri: url, json: req.body });
  } else {
    r = request(url);
  }

  req.pipe(r).pipe(res);
});

app.get("/sf-token", function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");

  let query = req.query;
  let username = query.username;
  let password = query.password;
  let prefix = query.prefix;

  conn.login(sf.username, sf.password, function(error, response) {
    if (error) {
      return console.error(error);
    }

    let postBody =
      '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">' +
      "<soapenv:Header>" +
      "<urn:LoginScopeHeader>" +
      "<urn:organizationId>" +
      conn.userInfo.organizationId +
      "</urn:organizationId>" +
      "</urn:LoginScopeHeader>" +
      "</soapenv:Header>" +
      "<soapenv:Body>" +
      "<urn:login>" +
      "<urn:username>" +
      username +
      "</urn:username>" +
      "<urn:password>" +
      password +
      "</urn:password>" +
      "</urn:login>" +
      "</soapenv:Body>" +
      "</soapenv:Envelope>";

    let reqPost = request(
      {
        url: conn.instanceUrl + "/services/Soap/u/39.0/",
        method: "POST",
        headers: {
          SOAPAction: "Login",
          "Content-Type": "text/xml"
        },
        data: "text/xml",
        body: postBody
      },
      function(error, xmlResponse, body) {
        parseString(body, function(err, data) {
          let sessionId = "";
          if (data["soapenv:Envelope"]["soapenv:Body"][0]["soapenv:Fault"]) {
            sessionId = "ERROR";
          } else {
            sessionId =
              data["soapenv:Envelope"]["soapenv:Body"][0]["loginResponse"][0][
                "result"
              ][0]["sessionId"][0];
          }
          res.json({ sessionId: sessionId });
        });
      }
    );
  });
});

const server = http.createServer(app);
server.listen(port, () => console.log(`API running on localhost:${port}`));

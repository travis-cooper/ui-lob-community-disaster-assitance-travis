import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';

import { CustomValidators } from 'ng2-validation';

import { GlobalService } from './../../services/global.service';
import { AppService } from './../../services/app.service';

import { ApiResponse } from './../../models/response.model';
import { IdentityContactRequest } from './../../models/identity-contact-request.model';

@Component({
	selector: 'cc-personal-info-form',
	templateUrl: './personal-info.component.html',
	styleUrls: [
		'./personal-info.component.css'
	],
	providers: [
		AppService
	]
})

export class PersonalInfoComponent implements OnInit {

	constructor(
		private globalService: GlobalService,
		private appService: AppService,
		private router: Router
	) { }

	personalInfoForm: FormGroup;
	modelPersonalInfo: PersonalInfo = new PersonalInfo();

	submitted = false;
	textAttempts = '';

	ngOnInit(): void {

		this.textAttempts = '';
		this.modelPersonalInfo.firstName = '';
		this.modelPersonalInfo.lastName = '';
		this.modelPersonalInfo.dob = null;
		this.modelPersonalInfo.ssn = '';

		this.generateForm();

	}

	generateForm(): void {

		const thisComponent = this;
		const group: any = {};
		Object.keys(this.modelPersonalInfo).forEach(key => {
			if (key === 'dob') {
				group[key] = new FormControl(this.modelPersonalInfo[key] || '', [Validators.required, CustomValidators.date]);
			} else {
				group[key] = new FormControl(this.modelPersonalInfo[key] || '', Validators.required);
			}

		});
		this.personalInfoForm = new FormGroup(group);

	}

	prepareCustomer(): IdentityContactRequest {

		const formModel: PersonalInfo = this.personalInfoForm.value;

		const dobDate: Date = new Date(formModel.dob);
		const formatDob: string = moment(dobDate).format('YYYY-MM-DD');
		const ssn4: string = formModel.ssn.slice(-4);

		const customer: IdentityContactRequest = new IdentityContactRequest({
			firstName: formModel.firstName,
			lastName: formModel.lastName,
			dob: formatDob,
			ssn: ssn4
		});

		return customer;

	}

	updateGlobalCustomer(): void {

		this.globalService.customer = Object.assign(this.globalService.customer, this.personalInfoForm.value);

	}

	submit(form): void {

		if (form.invalid || this.submitted) {
			return;
		}

		this.checkContact();

	}

	checkContact(): void {

		const payload: IdentityContactRequest = this.prepareCustomer();

		const thisComponent = this;
		thisComponent.submitted = true;

		this.appService.postIdentityContact(payload)
			.subscribe(
			response => {

				thisComponent.submitted = false;

				if (response.errorCode !== null) {
					this.handleErrors(response);
					return;
				}

				thisComponent.updateGlobalCustomer();
				thisComponent.router.navigate(['/address-info']);

			}
			);

	}

	handleErrors(errors: ApiResponse): void {

		switch (errors.errorCode) {
			case 400:
				this.handleAttempts(errors);
				break;
			default:
				this.router.navigate(['/verification-failed']);
				break;
		}

	}

	handleAttempts(errors: ApiResponse): void {

		const model: any = errors.models[0];
		const remainingAttempts: number = model.remainingAttempts;
		switch (remainingAttempts) {
			case 2:
				this.textAttempts = 'Incorrect, you have two more attempts.';
				break;
			case 1:
				this.textAttempts = 'Incorrect, you have one more attempt.';
				break;
			case 0:
				this.router.navigate(['/verification-failed']);
				break;
		}

	}

}

export class PersonalInfo {

	public firstName: string;
	public lastName: string;
	public dob: Date;
	public ssn: string;

}

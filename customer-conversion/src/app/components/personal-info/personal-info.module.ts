import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { CalendarModule, InputMaskModule } from 'primeng/primeng';
import { PersonalInfoComponent } from './personal-info.component';

@NgModule({
	declarations: [
		PersonalInfoComponent
	],
	imports: [
		FormsModule,
		ReactiveFormsModule,
		CalendarModule,
		InputMaskModule,
		CommonModule
	],
	exports: [
		PersonalInfoComponent
	]
})

export class PersonalInfoModule { }

import { Component, OnInit } from '@angular/core';

import { GlobalService } from './../../services/global.service';

@Component({
	selector: 'cc-verification-failed',
	templateUrl: './verification-failed.component.html',
	styleUrls: [
		'./verification-failed.component.css'
	]
})

export class VerificationFailedComponent implements OnInit {

	public supportPhone: string;
	public supportHours: string;

	constructor(
		private globalService: GlobalService
	) { }

	ngOnInit(): void {

		this.supportPhone = this.globalService.portalLabels.portal_support_phone;
		this.supportHours = this.globalService.portalLabels.portal_support_hours;

	}

}

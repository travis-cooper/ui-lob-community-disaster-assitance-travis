import { NgModule } from '@angular/core';
import { VerificationFailedComponent } from './verification-failed.component';

@NgModule({
	declarations: [
		VerificationFailedComponent
	],
	exports: [
		VerificationFailedComponent
	]
})

export class VerificationFailedModule { }

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { InputMaskModule } from 'primeng/primeng';
import { BusinessInfoComponent } from './business-info.component';

@NgModule({
	declarations: [
		BusinessInfoComponent
	],
	imports: [
		FormsModule,
		ReactiveFormsModule,
		InputMaskModule,
		CommonModule
	],
	exports: [
		BusinessInfoComponent
	]
})

export class BusinessInfoModule { }

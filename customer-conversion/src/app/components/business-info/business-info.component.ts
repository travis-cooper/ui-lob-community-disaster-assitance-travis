import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ApiResponse } from './../../models/response.model';
import { Business } from './../../models/business.model';

import { GlobalService } from './../../services/global.service';
import { AppService } from './../../services/app.service';

@Component({
	selector: 'cc-business-info',
	templateUrl: './business-info.component.html',
	styleUrls: [
		'./business-info.component.css'
	],
	providers: [
		AppService
	]
})

export class BusinessInfoComponent implements OnInit {

	constructor(
		private globalService: GlobalService,
		private appService: AppService,
		private router: Router
	) {

		this.generateForm();

	}

	businessInfoForm: FormGroup;
	businesses: Business[] = [];
	modelBusinessInfo: Business = new Business();
	usStates: string[] = this.globalService.usStates;
	legalStructure: {
		value?,
		label?
	}[] = [];
	industry: {
		value?,
		label?
	}[] = [];

	loading = false;
	submitted = false;
	currentBusinessIndex = 0;

	ngOnInit(): void {

		this.getBusinesses();

	}

	generateForm(): void {

		this.businessInfoForm = new FormGroup({
			'street': new FormControl('street', Validators.required),
			'street2': new FormControl('street2'),
			'city': new FormControl('city', Validators.required),
			'state': new FormControl('state', Validators.required),
			'zip': new FormControl('zip', Validators.required),
			'phone': new FormControl('phone', Validators.required),
			'legalStructure': new FormControl('', Validators.required),
			'industry': new FormControl('', Validators.required)
		});
	}

	getBusinesses(): void {

		const thisComponent = this;
		thisComponent.loading = true;

		this.appService.getBusinessInfo()
			.subscribe(
			response => {

				if (response.errorCode !== null) {
					this.handleErrors(response);
					return;
				}

				thisComponent.businesses = [];
				if (response.models) {
					response.models.map(model => {
						let business = new Business();
						business = { ...business, ...model };
						thisComponent.businesses.push(business);

						thisComponent.legalStructure = [];
						model.legalStructurePicklist.entries.map(item => {
							thisComponent.legalStructure.push({
								value: item.value,
								label: item.label
							})
						});
						thisComponent.industry = [];
						model.industryPicklist.entries.map(item => {
							thisComponent.industry.push({
								value: item.value,
								label: item.label
							})
						});

					});
				}

				thisComponent.currentBusinessIndex = 0;
				thisComponent.showNextBusiness();

			}
			);

	}

	showNextBusiness(): void {

		this.loading = true;

		if (this.businesses.length === 0 || this.currentBusinessIndex >= this.businesses.length) {
			location.reload();
			return;
		}

		const business: Business = this.businesses[this.currentBusinessIndex];
		this.businessInfoForm.controls.street.setValue(business.street);
		this.businessInfoForm.controls.street2.setValue(business.street2);
		this.businessInfoForm.controls.city.setValue(business.city);
		this.businessInfoForm.controls.state.setValue(business.state);
		this.businessInfoForm.controls.zip.setValue(business.zip);
		this.businessInfoForm.controls.phone.setValue(business.phone);
		this.businessInfoForm.controls.legalStructure.setValue(business.legalStructure);
		this.businessInfoForm.controls.industry.setValue(business.industry);

		this.modelBusinessInfo = { ...this.modelBusinessInfo, ...business};

		this.loading = false;

	}

	handleErrors(errors: ApiResponse): void {

		switch (errors.errorCode) {
			default:
				this.router.navigate(['/verification-failed']);
				break;
		}

	}

	submit(form): void {

		if (form.invalid || this.submitted) {
			return;
		}

		this.updateBusiness();

	}

	prepareBusiness(): Business {

		const formModel: Business = this.businessInfoForm.value;

		let business: Business = new Business();
		business = { ...business, ...formModel };
		business.recordId = this.modelBusinessInfo.recordId;

		return business;

	}

	updateBusiness(): void {

		const payload: Business = this.prepareBusiness();

		const thisComponent = this;
		thisComponent.submitted = true;

		this.appService.postBusinessInfo(payload)
			.subscribe(
			(response: ApiResponse) => {

				thisComponent.submitted = false;

				if (response.errorCode !== null) {
					this.handleErrors(response);
					return;
				}

				this.currentBusinessIndex++;
				this.showNextBusiness();

			}
			);
	}

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { GlobalService } from './../../services/global.service';
import { AppService } from './../../services/app.service';

import { ApiResponse } from './../../models/response.model';
import { QuestionBase } from './../../models/question-base.model';
import { RadioQuestion } from './../../models/question-radio.model';

@Component({
	selector: 'cc-identity-quiz',
	templateUrl: './identity-quiz.component.html',
	styleUrls: [
		'./identity-quiz.component.css'
	]
})

export class IdentityQuizComponent implements OnInit {

	constructor(
		private globalService: GlobalService,
		private appService: AppService,
		private router: Router
	) { }

	questions: QuestionBase<any>[] = [];
	quizForm: FormGroup = new FormGroup({});

	loading = false;
	submitted = false;

	ngOnInit(): void {

		this.getInitailQuiz();

	}

	getInitailQuiz(): void {

		const thisComponent = this;
		thisComponent.loading = true;

		this.appService.getIdentityQuiz()
			.subscribe(
			(response: ApiResponse) => {

				thisComponent.loading = false;

				if (response.errorCode !== null) {
					this.handleErrors(response);
					return;
				}

				thisComponent.generateQuestions(response);

			}
			);

	}

	generateForm(): void {

		const group: any = {};
		this.questions.forEach(question => {
			group[question.key] = new FormControl(question.value || '', Validators.required);
		});
		this.quizForm = new FormGroup(group);

	}

	generateQuestions(objResponse: any): void {

		this.questions = [];
		const objQuestions: any[] = objResponse.models[0].questions;
		for (const question of objQuestions) {

			const choices: {
				key: string,
				value: string
			}[] = [];
			for (const choice of question.choices) {
				choices.push({
					key: choice,
					value: choice
				});
			}
			this.questions.push(
				new RadioQuestion({
					key: 'question' + question.id,
					id: question.id,
					label: question.question,
					choices: choices
				})
			);
		}

		this.generateForm();

	}

	handleErrors(errors: ApiResponse): void {

		switch (errors.errorCode) {
			default:
				this.router.navigate(['/verification-failed']);
				break;
		}

	}

	submit(): void {

		if (this.quizForm.invalid || this.submitted) {
			return;
		}

		this.submitAnswers();

	}

	submitAnswers(): void {

		const payload: {
			answers: {
				id: number,
				answer: string
			}[]
		} = {
				answers: []
			};

		Object.keys(this.quizForm.value).forEach(key => {
			const id = parseInt(key.replace('question', ''), 10);
			payload.answers.push({
				id: id,
				answer: this.quizForm.value[key]
			});
		});

		const thisComponent = this;
		thisComponent.submitted = true;

		this.appService.postIdentityQuiz(payload)
			.subscribe(
			(response: ApiResponse) => {

				thisComponent.submitted = false;

				if (response.errorCode !== null) {
					this.handleQuizErrors(response);
					return;
				}

				thisComponent.router.navigate(['/business-info']);

			}
			);

	}

	handleQuizErrors(errors: ApiResponse): void {

		switch (errors.errorCode) {
			case 100:
				this.router.navigate(['/verification-failed']);
				break;
			case 101:
				this.router.navigate(['/verification-failed']);
				break;
			case 102:
				this.generateQuestions(errors);
				break;
			default:
				this.router.navigate(['/verification-failed']);
				break;
		}

	}
}

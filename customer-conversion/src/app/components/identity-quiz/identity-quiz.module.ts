import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { IdentityQuizComponent } from './identity-quiz.component';
import { DynamicFormQuestionComponent } from './../questions/dynamic-question.component';

@NgModule({
	declarations: [
		IdentityQuizComponent,
		DynamicFormQuestionComponent
	],
	imports: [
		FormsModule,
		ReactiveFormsModule,
		CommonModule
	],
	exports: [
		IdentityQuizComponent
	]
})

export class IdentityQuizModule { }

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { InputMaskModule } from 'primeng/primeng';
import { AddressInfoComponent } from './address-info.component';

@NgModule({
	declarations: [
		AddressInfoComponent
	],
	imports: [
		FormsModule,
		ReactiveFormsModule,
		InputMaskModule,
		CommonModule
	],
	exports: [
		AddressInfoComponent
	]
})

export class AddressInfoModule { }

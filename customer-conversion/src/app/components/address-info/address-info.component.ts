import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ApiResponse } from './../../models/response.model';
import { ProfileRequest } from './../../models/profile-request.model';
import { Customer } from './../../models/customer.model';
import { GlobalService } from './../../services/global.service';
import { AppService } from './../../services/app.service';

@Component({
	selector: 'cc-address-info',
	templateUrl: './address-info.component.html',
	styleUrls: [
		'./address-info.component.css'
	],
	providers: [
		AppService
	]
})

export class AddressInfoComponent implements OnInit {

	constructor(
		private globalService: GlobalService,
		private appService: AppService,
		private router: Router
	) {

		this.generateForm();

	}

	addressInfoForm: FormGroup;
	usStates: string[] = this.globalService.usStates;
	modelContact: any;

	loading = false;
	submitted = false;

	ngOnInit(): void {

		this.getContactInfo();

	}

	generateForm(): void {

		this.addressInfoForm = new FormGroup({
			'address1': new FormControl('address1', Validators.required),
			'address2': new FormControl('address2'),
			'city': new FormControl('city', Validators.required),
			'state': new FormControl('state', Validators.required),
			'zip': new FormControl('zip', Validators.required),
			'mobilePhone': new FormControl('mobilePhone', Validators.required),
			'altPhone': new FormControl('altPhone')
		});

	}

	getContactInfo(): void {

		const thisComponent = this;
		thisComponent.loading = true;

		this.appService.getProfile()
			.subscribe(
				(response: ApiResponse) => {

					thisComponent.loading = false;

					if (response.errorCode !== null) {
						this.handleErrors( response );
						return;
					}

					thisComponent.modelContact = response.models[0];

					this.addressInfoForm.controls.address1.setValue( thisComponent.modelContact.homeStreet );
					this.addressInfoForm.controls.address2.setValue( thisComponent.modelContact.homeStreet2 );
					this.addressInfoForm.controls.city.setValue( thisComponent.modelContact.homeCity );
					this.addressInfoForm.controls.state.setValue( thisComponent.modelContact.homeState );
					this.addressInfoForm.controls.zip.setValue( thisComponent.modelContact.homeZipCode );
					this.addressInfoForm.controls.mobilePhone.setValue( thisComponent.modelContact.mobilePhoneNumber );
					this.addressInfoForm.controls.altPhone.setValue( thisComponent.modelContact.phoneNumber );

				}
			);

	}

	submit(form): void {

		if (form.invalid || this.submitted) {
			return;
		}

		this.saveContact();

	}

	prepareProfile(): ProfileRequest {

		const formModel: Address = this.addressInfoForm.value;
		const customer: Customer = this.globalService.customer;

		const profile: ProfileRequest = this.modelContact;
		profile.firstName = customer.firstName;
		profile.lastName = customer.lastName;
		profile.homeStreet = formModel.address1;
		profile.homeStreet2 = formModel.address2;
		profile.homeCity = formModel.city;
		profile.homeState = formModel.state;
		profile.homeZipCode = formModel.zip;
		profile.mobilePhoneNumber = formModel.mobilePhone;
		profile.phoneNumber = formModel.altPhone;

		return profile;
	}

	saveContact(): void {

		const payload: ProfileRequest = this.prepareProfile();

		const thisComponent = this;
		thisComponent.submitted = true;

		this.appService.putProfile( payload )
			.subscribe(
				response => {

					if (response.errorCode !== null) {
						this.handleErrors( response );
						return;
					}

					this.runFraudRisk();
				}
			);

	}

	runFraudRisk(): void {

		const thisComponent = this;

		this.appService.postIdentityFraudRisk()
			.subscribe(
				response => {

					thisComponent.submitted = false;

					if (response.errorCode !== null) {
						this.handleErrors( response );
						return;
					}

					thisComponent.router.navigate(['/identity-quiz']);
				}
			);

	}

	handleErrors(errors: ApiResponse): void {

		switch (errors.errorCode) {
			default:
				this.router.navigate(['/verification-failed']);
				break;
		}

	}

}

export class Address {

	public address1: string;
	public address2: string;
	public city: string;
	public state: string;
	public zip: string;
	public mobilePhone: string;
	public altPhone: string;

}

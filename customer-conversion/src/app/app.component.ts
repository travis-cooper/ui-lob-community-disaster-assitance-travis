import { Component, OnInit } from '@angular/core';
import { environment } from './../environments/environment';

import { SalesforceService } from './services/salesforce.service';

@Component({
	selector: 'cc-root',
	templateUrl: './app.component.html',
	styleUrls: [ './app.component.css' ],
	providers: [
		SalesforceService
	]
})

export class AppComponent implements OnInit {

	imgWhiteTree = environment.sfDomainName + '/resource/portal_images/portal_images/navLogoFullWhite.png';

	constructor(
		private salesforceService: SalesforceService
	) {}

	ngOnInit(): void {

		// set the body background image dynamically
		document.body.style.backgroundImage = 'url("' + environment.sfDomainName + '/resource/portal_images/portal_images/login.jpg' + '")';

		if (environment.local) {

			this.salesforceService.login()
				.then(access_token => {
					console.log('Access token ::' , access_token);
					localStorage.setItem('SESSION_ID', access_token);
				});

		}

	}

}

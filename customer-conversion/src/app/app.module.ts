import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SFAuthInterceptor } from './sf-auth.interceptor';
import { CCGuard } from './services/cc-guard.service';
import { UtilityService } from './services/utility.service';
import { GlobalService } from './services/global.service';
import { AppService } from './services/app.service';
import { SalesforceService } from './services/salesforce.service';
import { AppRoutingModule } from './app-routing.module';
import { PersonalInfoModule } from './components/personal-info/personal-info.module';
import { AddressInfoModule } from './components/address-info/address-info.module';
import { IdentityQuizModule } from './components/identity-quiz/identity-quiz.module';
import { BusinessInfoModule } from './components/business-info/business-info.module';
import { VerificationFailedModule } from './components/verification-failed/verification-failed.module';

@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		AppRoutingModule,
		PersonalInfoModule,
		AddressInfoModule,
		IdentityQuizModule,
		BusinessInfoModule,
		VerificationFailedModule
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: SFAuthInterceptor,
			multi: true
		},
		UtilityService,
		CCGuard,
		GlobalService,
		AppService,
		SalesforceService
	],
	bootstrap: [ AppComponent ]
})
export class AppModule { }

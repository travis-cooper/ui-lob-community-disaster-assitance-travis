import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { GlobalService } from './global.service';

@Injectable()
export class CCGuard implements CanActivate {

	constructor(
		private router: Router,
		private globalService: GlobalService
	) {}

	canActivate() {

		if ( Object.keys(this.globalService.customer).length === 0 ) {

			this.router.navigate(['/personal-info']);
			return false;

		}

		return true;
	}

}

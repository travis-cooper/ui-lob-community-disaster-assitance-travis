import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/Observable/of';

import { UtilityService } from './../services/utility.service';
import { ApiResponse } from './../models/response.model';
import { IdentityContactRequest } from './../models/identity-contact-request.model';
import { ProfileRequest } from './../models/profile-request.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AppService {

	constructor(
		private http: HttpClient,
		private utilityService: UtilityService
	) {}

	getProfile(): Observable<ApiResponse> {

		const serviceUrl: string = this.utilityService.getServiceUrl('profile');
		return this.get(serviceUrl);

	}

	putProfile(payload: ProfileRequest): Observable<ApiResponse> {

		const serviceUrl: string = this.utilityService.getServiceUrl('profile');
		return this.put(serviceUrl, payload);

	}

	postIdentityContact(payload: IdentityContactRequest): Observable<ApiResponse> {

		const serviceUrl: string = this.utilityService.getServiceUrl('identity/contact');
		return this.post(serviceUrl, payload);

	}

	postIdentityFraudRisk(): Observable<ApiResponse> {

		const serviceUrl: string = this.utilityService.getServiceUrl('identity/fraud-risk');
		return this.post(serviceUrl, {});

	}

	getIdentityQuiz(): Observable<any> {

		const serviceUrl: string = this.utilityService.getServiceUrl('identity/quiz');
		return this.get(serviceUrl);

	}

	postIdentityQuiz(payload): Observable<any> {

		const serviceUrl: string = this.utilityService.getServiceUrl('identity/quiz');
		return this.post(serviceUrl, payload);

	}

	getBusinessInfo(): Observable<any> {

		const serviceUrl: string = this.utilityService.getServiceUrl('business-info');
		return this.get(serviceUrl);

	}

	postBusinessInfo(payload): Observable<any> {

		const serviceUrl: string = this.utilityService.getServiceUrl('business-info');
		return this.post(serviceUrl, payload);

	}

	get(serviceUrl: string): Observable<ApiResponse>  {

		return this.http
			.get<ApiResponse>(serviceUrl)
			.catch(res => {
				return Observable.of(res);
			});

	}

	post(serviceUrl: string, payload: any): Observable<ApiResponse> {

		return this.http
			.post<ApiResponse>(serviceUrl, payload)
			.catch(res => {
				return Observable.of(res);
			});

	}

	put(serviceUrl: string, payload: any): Observable<ApiResponse> {

		return this.http
			.put<ApiResponse>(serviceUrl, payload)
			.catch(res => {
				return Observable.of(res);
			});

	}

}

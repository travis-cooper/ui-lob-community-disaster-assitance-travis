import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

import { environment } from './../../environments/environment';

@Injectable()
export class UtilityService {

	public getServiceUrl(endpoint: string): string {

		let serviceUrl = '';
		const namespacePrefix = localStorage.getItem('NAMESPACE_PREFIX');
		if (environment.local) {
			serviceUrl = environment.proxyUri + 'services/apexrest/' + environment.sfPrefix + '/' + endpoint;
		} else {
			serviceUrl = environment.proxyUri + 'services/apexrest/' + (namespacePrefix ? namespacePrefix + '/' : '') + endpoint;
		}

		return serviceUrl;

	}

}

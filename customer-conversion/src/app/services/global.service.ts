import { Injectable } from '@angular/core';

import { Customer } from './../models/customer.model';

@Injectable()
export class GlobalService {

	public customer: Customer = new Customer();
	public usStates: string[] = [
		'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY',
		'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH',
		'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY', 'VI', 'PR'];
	public portalLabels: {
		portal_support_phone: string,
		portal_support_hours: string
	} = {
		portal_support_phone: '',
		portal_support_hours: ''
	};

	constructor(
	) {

		if (window['Portal'] && window['Portal']['Labels']) {
			this.portalLabels = window['Portal']['Labels'];
		}

	}

}

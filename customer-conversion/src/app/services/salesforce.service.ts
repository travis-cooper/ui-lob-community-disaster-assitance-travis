import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/toPromise';

import { environment } from './../../environments/environment';

@Injectable()
export class SalesforceService {

	constructor(
		private http: HttpClient
	) {}

	login(): Promise<any> {

		return new Promise((resolve, reject) => {

			this.http
				.get(environment.proxyUri + 'sf-token?username=' + environment.puUsername + '&password=' + environment.puPassword)
				.toPromise()
				.then(function(response) {
					resolve(response['sessionId']);
				});

		});

	}

}

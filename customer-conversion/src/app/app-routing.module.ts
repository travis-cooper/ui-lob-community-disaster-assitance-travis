import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CCGuard } from './services/cc-guard.service';

import { PersonalInfoComponent } from './components/personal-info/personal-info.component';
import { AddressInfoComponent } from './components/address-info/address-info.component';
import { IdentityQuizComponent } from './components/identity-quiz/identity-quiz.component';
import { BusinessInfoComponent } from './components/business-info/business-info.component';
import { VerificationFailedComponent } from './components/verification-failed/verification-failed.component';


const routes: Routes = [
	{ path: '', redirectTo: 'personal-info', pathMatch: 'full' },
	{ path: 'personal-info', component: PersonalInfoComponent },
	{ path: 'address-info', component: AddressInfoComponent, canActivate: [CCGuard] },
	{ path: 'identity-quiz', component: IdentityQuizComponent, canActivate: [CCGuard] },
	{ path: 'business-info', component: BusinessInfoComponent, canActivate: [CCGuard] },
	{ path: 'verification-failed', component: VerificationFailedComponent },
	{ path: '**', redirectTo: 'personal-info', pathMatch: 'full' }
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { useHash: true })
	],
	exports: [
		RouterModule
	]
})

export class AppRoutingModule { }

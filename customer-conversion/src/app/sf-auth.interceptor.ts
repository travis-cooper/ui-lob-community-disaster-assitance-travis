import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/do';

import { UtilityService } from './services/utility.service';
import { environment } from './../environments/environment';

@Injectable()
export class SFAuthInterceptor implements HttpInterceptor {

	constructor(
		private utilityService: UtilityService
	) {}

	private sessionId: string = localStorage.getItem('SESSION_ID') || '';
	private oSessionId: string = localStorage.getItem('O_SESSION_ID') || '';

	intercept(req: HttpRequest<any>, next: HttpHandler) {

		const sfReq = req.clone({
			headers: this.prepareHeaders(req),
			body: this.prepareBody(req)
		});

		return next.handle(sfReq)
			.do(
				(event: HttpEvent<any>) => {
					// do something with response
				},
				(error: any) => {
					alert('Request error. Please try again');
				}
			);

	}

	private prepareHeaders(req: HttpRequest<any>): HttpHeaders {

		let headers: HttpHeaders = req.headers || new HttpHeaders();

		if (environment.local && req.url.indexOf('sf-token') > -1) {

		} else {
			headers = headers.set('Content-Type', 'application/json');
			headers = headers.set('Authorization', 'Bearer ' + this.sessionId);
			headers = headers.set('oSessionId', this.oSessionId);
			headers = headers.set('X-User-Agent', 'salesforce-toolkit-rest-javascript/v40.0');
			headers = headers.set('Version', '2.0');
		}

		return headers;

	}

	private prepareBody(req: HttpRequest<any>): string {

		let body = req.body || '';

		if (body && typeof body === 'object') {
			body = JSON.stringify(body);
		}

		return body;
	}

}

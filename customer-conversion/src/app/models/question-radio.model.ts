import { QuestionBase } from './question-base.model';

export class RadioQuestion extends QuestionBase<string> {

	controlType = 'radio';
	choices: {
		key: string,
		value: string
	}[] = [];

	constructor(options: {} = {}) {

		super(options);
		this.choices = options['choices'] || [];

	}

}

interface IServiceModel {}

export class ApiResponse {
	public errorCode: number;
	public statusMessage: string;
	public models: IServiceModel[];
}

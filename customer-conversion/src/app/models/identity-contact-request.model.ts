export class IdentityContactRequest {

	firstName: string;
	lastName: string;
	dob: string;
	ssn: string;

	constructor (options: {
		firstName?: string;
		lastName?: string;
		dob?: string;
		ssn?: string;
	} = {}) {

		this.firstName = options.firstName;
		this.lastName = options.lastName;
		this.dob = options.dob;
		this.ssn = options.ssn;

	}
}

export class QuestionBase<T> {

	value: T;
	key: string;
	id: number;
	label: string;
	controlType: string;

	constructor(options: {
		value?: T,
		key?: string,
		id?: number,
		label?: string,
		controlType?: string
	} = {}) {

		this.value = options.value;
		this.key = options.key;
		this.id = options.id;
		this.label = options.label || '';
		this.controlType = options.controlType || '';

	}

}

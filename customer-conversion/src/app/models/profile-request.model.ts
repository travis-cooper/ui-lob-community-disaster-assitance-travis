export class ProfileRequest {

	emailAddress: string;
	firstName: string;
	middleName: string;
	lastName: string;
	homeStreet: string;
	homeStreet2: string;
	homeCity: string;
	homeState: string;
	homeZipCode: string;
	mobilePhoneNumber: string;
	phoneNumber: string;

	constructor (options: {
		emailAddress?: string;
		firstName?: string;
		middleName?: string;
		lastName?: string;
		homeStreet?: string;
		homeStreet2?: string;
		homeCity?: string;
		homeState?: string;
		homeZipCode?: string;
		mobilePhoneNumber?: string;
		phoneNumber?: string;

	} = {}) {

		this.emailAddress = options.emailAddress;
		this.lastName = options.lastName;
		this.homeStreet = options.homeStreet;
		this.homeStreet2 = options.homeStreet2;
		this.homeCity = options.homeCity;
		this.homeState = options.homeState;
		this.homeZipCode = options.homeZipCode;
		this.mobilePhoneNumber = options.mobilePhoneNumber;
		this.phoneNumber = options.phoneNumber;

	}
}

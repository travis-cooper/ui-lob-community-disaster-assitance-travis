export class Customer {

	public firstName: string;
	public lastName: string;
	public dob: Date;
	public ssn: string;
	public addressInfo: AddressInfo;

}

export class AddressInfo {

	public address1: string;
	public address2: string;
	public city: string;
	public state: string;
	public zip: string;
	public mobilePhone: string;
	public altPhone: string;

}

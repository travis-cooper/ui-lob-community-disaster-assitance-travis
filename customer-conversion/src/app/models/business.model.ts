export class Business {

	public recordId: string;
	public name: string;
	public street: string;
	public street2: string;
	public city: string;
	public state: string;
	public zip: string;
	public phone: string;
	public legalStructure: string;
	public industry: string;

}

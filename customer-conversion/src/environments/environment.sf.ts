export const environment = {
	production: false,
	local: false,
	sfDomainName: '',
	sfPrefix: '',
	puUsername: '',
	puPassword: '',
	proxyUri: '/'
};

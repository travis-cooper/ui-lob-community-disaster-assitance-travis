export const environment = {
	production: false,
	local: true,
	sfDomainName: '<YOUR_DOMAIN_NAME>',
	sfPrefix: '<YOUR_SALESFORCE_PREFIX>',
	puUsername: '<YOUR_USERNAME>',
	puPassword: '<YOUR_PASSWORD>',
	proxyUri: 'http://localhost:3000/'
};

export const environment = {
	production: true,
	local: false,
	sfDomainName: '',
	sfPrefix: '',
	puUsername: '',
	puPassword: '',
	proxyUri: '/'
};

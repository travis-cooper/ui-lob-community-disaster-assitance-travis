var fs = require("fs");
var builder = require('xmlbuilder');

var dirStaticResources = './dist/staticresources';
var dirPages = './dist/pages';
var dirPortalStaticResource = './../src/staticresources';
var dirPortalPages = './../src/pages';

var processStaticResourceJS = function(filename) {
    
    var escFilename = filename.replace(/\./g, '_');
    var origFn = "./dist/" + filename;
    var distFn = dirStaticResources + "/" + escFilename + ".resource";
    var distFnSf = dirPortalStaticResource + "/" + escFilename + ".resource";
    fs.renameSync(origFn, distFn);
    fs.createReadStream(distFn).pipe(fs.createWriteStream(distFnSf));

    var xmlMatadata = builder.create('StaticResource', {version: '1.0', encoding: 'UTF-8'})
            .att('xmlns', 'http://soap.sforce.com/2006/04/metadata')
                .ele('cacheControl', {}, 'Public')
                .up()
                .ele('contentType', {}, 'text/javascript')
            .end({ pretty: true});

    origFn = dirStaticResources + "/" + escFilename + ".resource-meta.xml";
    distFnSf = dirPortalStaticResource + "/" + escFilename + ".resource-meta.xml";
    fs.writeFileSync(origFn, xmlMatadata.toString());
    fs.createReadStream(origFn).pipe(fs.createWriteStream(distFnSf));
};

if (!fs.existsSync(dirStaticResources)) {
    fs.mkdirSync(dirStaticResources);
};
processStaticResourceJS("inline.bundle.js");
processStaticResourceJS("polyfills.bundle.js");
processStaticResourceJS("styles.bundle.js");
processStaticResourceJS("vendor.bundle.js");
processStaticResourceJS("main.bundle.js");

fs.createReadStream(dirPages + '/IdentityVerification.page').pipe(fs.createWriteStream(dirPortalPages + '/IdentityVerification.page'));
fs.createReadStream(dirPages + '/IdentityVerification.page-meta.xml').pipe(fs.createWriteStream(dirPortalPages + '/IdentityVerification.page-meta.xml'));

// create a textfile with a list of the customer-conversion files
var dirRoot = 'customer-conversion/';
var list = '';
fs.readdirSync(dirStaticResources).forEach(file => {
    list += 'src/staticresources/' + file + '\n';
});
fs.readdirSync(dirPages).forEach(file => {
    list += 'src/pages/' + file + '\n';
});
fs.writeFileSync('cc-files.txt', list);
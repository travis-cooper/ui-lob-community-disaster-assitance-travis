var gulp = require('gulp');
var zip = require('gulp-zip');
var forceDeploy = require('gulp-jsforce-deploy');
var fs = require("fs");

var getCreds = function() {
    var fileContent = fs.readFileSync('./../build.properties', 'utf8');
    var q = new RegExp(/sf.username\s=\s(\S+)\nsf.password\s=\s(\S+)/);
    var r = q.exec(fileContent);
    var username = r[1].trim();
    var password = r[2].trim();

    return {username: username, password: password}
}

var sf = getCreds();

gulp.task('deploy', function() {
	gulp.src('./dist/**', { base: "." })
		.pipe(zip('dist.zip'))
		.pipe(forceDeploy({
			username: sf.username,
			password: sf.password,
			version: '40.0',
			verbose: true
		}));
});
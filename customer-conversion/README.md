# CustomerConversion

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.3.1.

You need to update the info in `src/environments/environment.ts`.

## Development server

Run `node server.js` for a dev proxy server. 

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Deploy to Salesfroce

Run `npm run deploy` to deploy the project to your Salesforce org.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

#!/bin/sh

# Variables section
ORIG_DIR=$(dirname "$0")
TEMPLATES_DIR="templates"
TARGET_DIR="src"
TMP_DIR=".tmp"
PLACEHOLDER="AppName"
PLACEHOLDER_LC="appname"

# Ensure the app name has been provided
if [ -z "$1" ]; then
	echo "Usage: $0 AwesomeAppName" 1>&2
	exit 1
fi;

# Resolve the app name
APP_NAME="$1"
APP_NAME_LC=$(echo "$APP_NAME" | tr '[:upper:]' '[:lower:]')
echo "Generating application: $APP_NAME ..."

# Check if temp dir exists and if so, remove it and create it again
if [ -d "$TMP_DIR" ]; then
	rm -rf "$TMP_DIR"
fi
mkdir -p "$TMP_DIR"

# Copy files from the template dir to the tmp directory
cp -rf $TEMPLATES_DIR/* $TMP_DIR

# Rename every file to replace the placeholder with the application name
find $TMP_DIR -type f -name "*$PLACEHOLDER*" | while read FILE ; do
	newfile=`echo "$FILE" | sed -e "s/$PLACEHOLDER/$APP_NAME/"` ;
	mv "$FILE" "$newfile" ;
	echo "$newfile" | sed -e "s/$TMP_DIR\\///" ;
done
find $TMP_DIR -type f -name "*$PLACEHOLDER_LC*" | while read FILE ; do
	newfile=`echo "$FILE" | sed -e "s/$PLACEHOLDER_LC/$APP_NAME_LC/"` ;
	mv "$FILE" "$newfile" ;
	echo "$newfile" | sed -e "s/$TMP_DIR\\///" ;
done

PLACEHOLDER="%!${PLACEHOLDER}%"
PLACEHOLDER_LC="%!${PLACEHOLDER_LC}%"

# Replace the placeholders with the application name in files contents
find $TMP_DIR -type f -exec sed -i '' "s/$PLACEHOLDER/$APP_NAME/g" {} +
find $TMP_DIR -type f -exec sed -i '' "s/$PLACEHOLDER_LC/$APP_NAME_LC/g" {} +

cp -rf $TMP_DIR/* $TARGET_DIR

# Remove temp dir
rm -rf "$TMP_DIR"

echo "Done"
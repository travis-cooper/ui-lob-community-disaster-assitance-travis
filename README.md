# Rest Starter Template

## Overview

Template repository `starter` contains set of main classes and configuration files required to create banking components.

We use bitbucket forking to create implementations of ReST services. Forking allows easy initial copy of the original template. In addition, this documentation contains steps to update set of dependencies via `git archive`.

The QA team will create and configure a source code repository in api-microservices project in Bitbucket.
Each repository should have Bitbucket pipelines enabled.  Settings -> Pipelines -> Settings -> Enable Pipelines.
Each repository should have branch permissions configured on the master branch.

## Before Issuing a Pull Request, run CI locally
```
% commit and push all your changes to your branch in bitbucket.
# Run CI locally with this command.
% ./deploy-ci.sh

```
- Create a Developer Edition Salesforce Org at https://developer.salesforce.com/signup
- Setup → Manage Users → Users → Select your name → Edit → Set role to "CEO" and check "Knowledge User".
- Setup → Email Administration → Deliverability and set "Access Level" picklist to "System email only".
- Setup → Customize → Communities → Communities Settings → Enable communities
Choose a domain name prefix.  portal-dev-trb [ developer-edition ] (this goes to COMMUNITY_URL_PREFIX and COMMUNITY_URL_PREFIX_UNDERSCORE (replace `-` with `_`) variables in .env)
- Setup → Security Controls → Sharing Settings and then set 'Account and Contract' `Default Internal Access` as `Private`.
Then Enable External Sharing Model (This button may be initially disabled after the previous step. Wait a few minutes and refresh the page until it becomes clickable).
- Setup → Account Teams → Enable
- Setup → Customize → Knowledge Settings → Enable Salesforce Knowledge 
- Reset the Security Token. My Settings → Personal → Reset My Security Token (this may take a while to be sent to your email)
- Enter following data into the .env file:
  - PROJECT_NAME
  - SF_USER_ID
  - SF_PASSWORD which contains <portal password><security token from sales force>
  - COMMUNITY_URL_PREFIX 

## Installer commands overview

Before any command can be executed, you need to install npm dependencies with:

```
npm install
```

There are two types of commands:
  - Basic commands
  - Aggregated commands

Basic commands are main building blocks and represent simple command. For example: bring up-to-date dependency for LOB package or install BankingServices in SalesForce Org. 

Aggregated commnds are sets of basic commands that execute one after another. If one fails, aggregated command stops.

To execute a any command run the following:

```bash
npm run <command>
```

### List of Aggregated commands

Aggregated commands create fully configured SalesForce Org that is tailored for specific type of use. Below is list of currently supported commands:

 - build-packaging-org: Creates packaging org.
 - build-developer-org: Creates SF org for development of ReST micro services. 
 - build-ui-dev-org:    Creates SF org for development of UI components (UI_LOB_PORTAL package)


### List of Basic commands

- build-properties: creates a `ant` configuration file `build.properties` from details such as user and password from `.env`
- dependency-LLC_BI: gets up-to-date nCino Platform dependency from `starter-templates`
- dependency-nDESIGN: gets up-to-date nCino Layout dependency from `starter-templates`
- dependency-nFORCE: gets up-to-date nFORCE dependency from `starter-templates`
- dependency-LiveOAK: gets up-to-date LiveOak dependency from `starter-templates`
- dependency-LOB_TDF: gets up-to-date Test Data Factory dependency from `starter-templates`
- dependency-API_TDF: gets up-to-date Test Data Factory APIs dependency from `starter-templates`
- dependency-API_CHR: gets up-to-date Rest Chronology package dependency from `starter-templates`
- config-services: starts Banking Services that will also initialize nCino bean objects in the Sales Force Org. In particular, `LiveOak.BankingServicesDataProvider` and `nForce.LifeCycleDao`.
- upload-sfdc-deposits-portal-metadata: loads templates for Deposit Portal Meta data configurations from `starter-templates` repo.
- build-sfdc-deposits-portal-metadata: generates code in /src for Deposit Portal Meta data configurations. This will be later deployed with `and deployCode`
- configure-sfdc-deposits-portal-metadata: makes REST call to the org and runs API_TDF commands to create Alberta and 3 bank accounts: checking, savings and CD.


#### Create Developer Org for Microservice

```
npm install
npm run build-developer-org
```

#### Create Packaging Org

```
npm install
npm run build-packaging-org
```

## Manually update installer

Rather than update entire project from repo, it is possible to simply refresh installer scrip with the steops below;

```shell
git archive --format=tar --remote git@bitbucket.org:liveoakbank/starter.git HEAD package.json | tar xf -
git archive --format=tar --remote git@bitbucket.org:liveoakbank/starter.git HEAD installer.js | tar xf -
```

## Run build pipeline locally

A script called docker-deploy-ci.sh is provided which will checkout a project within a docker container, deploy code to the CI org and run test cases.

Calling convention is as follows:
./docker-deploy-ci.sh [repo-slug] [branch-name]
where [repo-slug] is the name of your repository and [branch-name] is the branch you'd like to have deployed to the CI org


#!/bin/sh

# Variables section
TMPDIR=/tmp/ui-portal-dev
ORIGDIR=$(dirname "$0")
REPOSITORY_URL=`git config --get remote.origin.url`
DEPENDENCIES_DIR="dependencies/installedPackages"

while true; do
	# Get current git branch and validate it's a valid git repo
	BRANCH_NAME="$(git rev-parse --abbrev-ref HEAD)"
	if [ "$?" = "0" ]; then
		echo "Using repository:: $REPOSITORY_URL"
		echo "Using branch: $BRANCH_NAME"
	else
		echo "Not a valid git repository! Aborting" 1>&2
		exit 1
	fi;

	# Confirm the branch to deploy to CI
	read -p "Is this the branch that you want to deploy to CI? [y/n] > " yn
	case $yn in
		[Yy]* ) break;;
		[Nn]* ) echo "Please change your branch and try again. List of branches in local repo is:\n";
			git branch -l;
			echo "";
			exit;;
		* ) echo "Please answer yes or no.";
	esac
done

# Check for existing tmp dir and if so remove it, then create it
if [ -d "$TMPDIR" ]; then
	printf '%s\n' "Removing existing directory: $TMPDIR"
	rm -rf "$TMPDIR"
fi
mkdir -p "$TMPDIR"

# Try to clone git repo into the tmp dir
echo "Cloning repo into directory: $TMPDIR"
git clone -b "$BRANCH_NAME" "$REPOSITORY_URL" "$TMPDIR"
if [ "$?" = "0" ]; then
	echo "Repo downloaded successfuly!"
else
	echo "Error while fetching remote branch! Aborting" 1>&2
	exit 1
fi

# Remove uneeded dependencies (TODO: Put these in an array or something)
rm -f "$TMPDIR/$DEPENDENCIES_DIR/LLC_BI.installedPackage"
rm -f "$TMPDIR/$DEPENDENCIES_DIR/nFORCE.installedPackage"
rm -f "$TMPDIR/$DEPENDENCIES_DIR/nDESIGN.installedPackage"

# Change to tmp dir and execute the ant deploy command
echo "Deploying to CI ..."
cd "$TMPDIR"
ant deployCI

# Remove tmp dir
rm -rf "$TMPDIR"

# Change back to the original directory
cd "$ORIGDIR"

global with sharing class PortalService {

	global String localNamespace  {get; set;}
	global String pkgNamespace  {get; set;}
	global String portalErrorRedirectURL {get; set;}
	global String portalErrorDetailLevel {get; set;}
	global String portalErrorMessage {get; set;}
	global String currentIPAddress   { get; private set; }
	global String environment   { get; private set; }
	global String apiVersion { get; private set; }
	global String oSessionId;
	global String portalSupportPhone { get; private set; }
	global String portalSupportHours { get; private set; }
	global String ccPage { get; private set; }
	global String portalPage { get; private set; }


	global PortalService(nFORCE.LifeCycleAppController controller){
		init();
	}
	global PortalService(){
		init();
	}

	global void onInit() {
		setSessionDetails();

	}

	private void init(){
		this.localNamespace = [SELECT NamespacePrefix FROM Organization].NamespacePrefix;
		this.pkgNamespace = LiveOak.APITools.getInstance().namespacePrefix;
		this.portalErrorRedirectURL = LiveOak.PortalSetting.get.value('Portal_Error_Redirect');
		this.portalErrorDetailLevel = LiveOak.PortalSetting.get.value('Portal_Error_Detail_Level');
		this.portalErrorMessage = LiveOak.PortalSetting.get.value('Portal_Error_Message');
		this.currentIPAddress = GetUserIPAddress();
		this.environment = LiveOak.PortalSetting.get.value(Label.LiveOak.Portal_Setting_Environment);
		this.apiVersion = LiveOak.PortalSetting.get.value(Label.LiveOak.API_Version_Number);
		this.portalSupportPhone = LiveOak.PortalSetting.get.value(Label.LiveOak.Portal_Setting_Support_Phone_Label);
		this.portalSupportHours = LiveOak.PortalSetting.get.value(Label.LiveOak.Portal_Setting_Support_Hours_Label);

	}

	global String GetUserIPAddress() {
		string returnValue = '';
		if(ApexPages.currentPage() != null) {
			returnValue = ApexPages.currentPage().getHeaders().get('True-Client-IP');
			if (String.isBlank(ReturnValue)) {
				returnValue = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');

			}

		}else if(RestContext.request != null) {
			returnValue = RestContext.request.remoteAddress;

		}
		return returnValue;

	}

	global String getOSessionId(){
		if(this.oSessionId == null) {
			setSessionDetails();
		}
		return this.oSessionId;

	}

	global void setSessionDetails() {

		String sessionId = UserInfo.getSessionId();
		String ip        = this.GetUserIpAddress();
		String userAgent = '';
		if(ApexPages.currentPage() != null) {
			userAgent = ApexPages.currentPage().getHeaders().get('User-Agent');
		}
		String methodId  = '';
		String deviceId  = '';

		this.oSessionId = LiveOak.APITools.createSessionDetails(ip, userAgent, methodId, deviceId, null, UserInfo.getUserId());

	}

	global static LiveOak.Model_Attachment saveAttachment(Id parentId, String fileName, String contentType, String body, Boolean overwriteExisting)
	{
		LiveOak.Model_Attachment modelAttachment = getAttachment(parentId, fileName);

		if (modelAttachment.getId() != null && !overwriteExisting) {
			return modelAttachment;
		}

		Blob blobBody = EncodingUtil.base64Decode(body);
		Attachment attachment = new Attachment (
			ParentId    = parentId,
			Name        = fileName,
			ContentType = contentType,
			Body        = blobBody
			);

		insert attachment;

		modelAttachment = new LiveOak.Model_Attachment();
		modelAttachment.setDbObject(attachment);
		modelAttachment.setId(attachment.Id);
		modelAttachment.mapFromDb();

		return modelAttachment;
	}

	global static LiveOak.Model_Attachment saveBlankAttachment(Id parentId, String fileName, String contentType, Boolean overwriteExisting)
	{
		LiveOak.Model_Attachment modelAttachment = getAttachment(parentId, fileName);

		if (modelAttachment.getId() != null && !overwriteExisting) {
			return modelAttachment;
		}

		Blob blobBody = EncodingUtil.base64Decode('BLANK');
		Attachment attachment = new Attachment (
			ParentId    = parentId,
			Name        = fileName,
			ContentType = contentType,
			Body        = blobBody
			);

		insert attachment;

		modelAttachment = new LiveOak.Model_Attachment();
		modelAttachment.setDbObject(attachment);
		modelAttachment.setId(attachment.Id);
		modelAttachment.mapFromDb();

		return modelAttachment;
	}

	global static LiveOak.Model_Attachment appendChunkAttachment(Id attachmentId, String body)
	{

		Blob blobBody = EncodingUtil.base64Decode(body);
		Attachment attachment = [SELECT Id, Body FROM Attachment WHERE Id = :attachmentId ];
		attachment.Body = EncodingUtil.convertFromHex( EncodingUtil.convertToHex(attachment.Body) + EncodingUtil.convertToHex(blobBody) );
		update attachment;

		LiveOak.Model_Attachment modelAttachment = new LiveOak.Model_Attachment();
		modelAttachment.setDbObject(attachment);
		modelAttachment.setId(attachment.Id);
		modelAttachment.mapFromDb();

		return modelAttachment;
	}

	global static LiveOak.Model_Attachment buildAttachment(Id parentId, String fileName, String contentType, String body, Boolean overwriteExisting, Boolean decodeBody) {
		LiveOak.Model_Attachment modelAttachment = getAttachment(parentId, fileName);
		Blob blobBody;

		if (modelAttachment.getId() != null && !overwriteExisting) {
			return modelAttachment;
		}

		if (decodeBody) {
			blobBody = EncodingUtil.base64Decode(body);
		} else {
			blobBody = Blob.valueOf(body);
		}

		Attachment attachment = new Attachment (
			ParentId    = parentId,
			Name        = fileName,
			ContentType = contentType,
			Body        = blobBody
			);

		modelAttachment = new LiveOak.Model_Attachment();
		modelAttachment.setDbObject(attachment);
		modelAttachment.mapFromDb();

		return modelAttachment;
	}

	global static List<LiveOak.Model_Attachment> buildCalloutAttachments(List<LiveOak.Model_APICallout> callouts){

		List<LiveOak.Model_Attachment> attachments = new List<LiveOak.Model_Attachment>();
		Set<String> existingAttachement = new Set<String>();

		Set<Id> calloutIds = new Set<Id>();
		for(LiveOak.Model_APICallout callout: callouts) { calloutIds.add(callout.getId()); }
		List<Attachment> dbAttachments = [ SELECT Id FROM Attachment WHERE ParentId IN: calloutIds ];

		if(dbAttachments.size()>0) {
			List<Id> attachementIds = new List<Id>(nFORCE.Utility.getObjectIds(dbAttachments));
			attachments.addAll((List<LiveOak.Model_Attachment>)LiveOak.IForceService.retrieveIForces(attachementIds));
		}

		for(LiveOak.Model_Attachment attachment :attachments) {
			existingAttachement.add(attachment.ParentId+''+attachment.getName());
		}

		String key;
		LiveOak.Model_Attachment modelAttachment;
		for(LiveOak.Model_APICallout callout: callouts) {
			key = callout.getId()+Label.LiveOak.API_CALLOUT_REQUEST_ATTACHMENT_FILENAME;
			if(!existingAttachement.contains(key) && !String.isBlank(callout.requestBody)) {
				attachments.add(new LiveOak.Model_Attachment(
							callout.getId(),
							Label.LiveOak.API_CALLOUT_REQUEST_ATTACHMENT_FILENAME,
							Label.LiveOak.API_CALLOUT_REQUEST_CONTENT_TYPE,
							Blob.valueOf(callout.requestBody))
				                );
			}

			key = callout.getId()+Label.LiveOak.API_CALLOUT_RESPONSE_ATTACHMENT_FILENAME;
			if(!existingAttachement.contains(key) && !String.isBlank(callout.responseBody)) {
				attachments.add(new LiveOak.Model_Attachment(
							callout.getId(),
							Label.LiveOak.API_CALLOUT_RESPONSE_ATTACHMENT_FILENAME,
							Label.LiveOak.API_CALLOUT_RESPONSE_CONTENT_TYPE,
							Blob.valueOf(callout.responseBody)
							));
			}
		}

		return attachments;
	}

	global static LiveOak.Model_Attachment getAttachment(String parentId, String fileName)
	{
		List<Attachment> attachments = [
			SELECT Id, Name, ParentId, ContentType, Body, Description
			FROM Attachment
			WHERE ParentId = :parentId AND Name = :fileName
			                                      ORDER BY LastModifiedDate DESC
			                                      LIMIT 1
		];

		LiveOak.Model_Attachment modelAttachment = new LiveOak.Model_Attachment();

		if (attachments.size() > 0) {
			modelAttachment.setDbObject(attachments[0]);
			modelAttachment.setId(attachments[0].Id);
			modelAttachment.mapFromDb();
		}

		return modelAttachment;
	}


	global static void deleteAttachment(String parentId, String fileName) {
		List<Attachment> attachments = [
			SELECT Id, Name, ParentId, ContentType, Body, Description
			FROM Attachment
			WHERE ParentId = :parentId AND Name = :fileName
		];

		if (attachments.size() > 0) {
			delete attachments;
		}
	}

	global static void deleteAttachmentsForParent(String parentId) {
		List<Attachment> attachments = [
			SELECT Id
			FROM Attachment
			WHERE ParentId = :parentId
		];

		if (attachments.size() > 0) {
			delete attachments;
		}
	}

	private final static LiveOak.IAPILogger logger = LiveOak.APILogger.getLogProvider(Label.LiveOak.DebugLogger, PortalService.class);

}
@isTest
public class TestPortalController {
	@isTest
	public static void testPortalController(){
		LOB_TDF.TestDataFactoryHelper helper = LOB_TDF.TestDataFactoryHelper.getInstance();

		System.runAs(helper.getPortalUser(LOB_TDF.TestDataFactoryHelper.PORTAL_USER_USERNAME)){
			PortalController controller = new PortalController();
			System.assert(controller != null);
			System.assertEquals(
				PortalController.mxInstitutionGuid, 
				LiveOak.PortalSetting.get.value(Label.LiveOak.Portal_Setting_Portal_Institution_Guid_Label)
			);
			System.assertEquals(0, PortalController.loginCount);

		}

	}


	@isTest
	public static void testPortalService(){
		LOB_TDF.TestDataFactoryHelper helper = LOB_TDF.TestDataFactoryHelper.getInstance();
		System.runAs(helper.getPortalUser(LOB_TDF.TestDataFactoryHelper.PORTAL_USER_USERNAME)){
			List<LiveOak.Model_Attachment> attachments = createAttachments();
			PortalService controller = new PortalService();
			System.assert(controller != null);
			System.assert(controller.portalSupportPhone != null);
			controller.getOSessionId();
			controller.onInit();
			List<LiveOak.Model_APICallout> callouts = new List<LiveOak.Model_APICallout>();
			LiveOak.Model_APICallout callout = new LiveOak.Model_APICallout();
			callouts.add(callout);
			List<LiveOak.Model_Attachment> calloutAttachments = PortalService.buildCalloutAttachments(callouts);
			LiveOak.Model_Attachment attachmentToExercise = attachments.get(0);
			LiveOak.Model_Attachment builtAttachment = PortalService.buildAttachment(attachmentToExercise.parentId, 'filename1', attachmentToExercise.contentType, '', false, false);
			LiveOak.Model_Attachment blankAttachment = PortalService.saveBlankAttachment(attachmentToExercise.parentId, 'filename2', attachmentToExercise.contentType, false);
			PortalService.deleteAttachment(attachmentToExercise.parentId, 'filename2');
			PortalService.deleteAttachmentsForParent(attachmentToExercise.parentId);

		}

	}

	private static List<LiveOak.Model_Attachment> createAttachments() {
		LiveOak.Model_Customer customer = (LiveOak.Model_Customer)LiveOak.IForceService.retrieveIForce(UserInfo.getUserId());
		List<LiveOak.Model_Attachment> toReturn = new List<LiveOak.Model_Attachment>();

		List<String> attachmentNames = new List<String>{'TestAttachment1.txt', 'TestAttachment2.txt', 'TestAttachment3.txt'};
		for (String fileName : attachmentNames) {
			toReturn.add(
				PortalService.saveAttachment(
					customer.accountId, 
					fileName, 
					LiveOak.TestUtility.ATTACHMENT_CONTENTTYPE_TEXT, 
					LiveOak.TestUtility.ATTACHMENT_CONTENT_BASE64, true
					)
				);

		}

		return toReturn;

	}

}
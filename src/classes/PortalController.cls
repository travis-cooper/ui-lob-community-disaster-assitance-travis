/**
 * Class to serve as the MVC controller for Customer Portal.
 *
 * @class PortalController
 *
 */

global with sharing class PortalController {

	// ----------------------------------------------------------------------
	// PortalController Constructor
	// ----------------------------------------------------------------------
	global Datetime dt {get; set;}
	global String UserSessionID {get; set;}
	global static String ThreatMetrixSessionId {get; private set;}
	global static String OrgId {get; private set;}
	global Id accountId {get; set;}
	global Integer sessionNotificationTimeout { get; set; }
	global Integer sessionConfirmationTimeout { get; set; }

	global String prevLocation {get; set;}

	global static String legalEntityLoans { get; private set; }

	global static final String FILE_URL_PATH  = '/servlet/servlet.ImageServer?oid=' + UserInfo.getOrganizationId() + '&id=';

	global static Boolean showDashboard         { get; private set; }
	global static Boolean showReferrals         { get; private set; }
	global static Boolean showLoanApplication   { get; private set; }
	global static Boolean showBenchmarks        { get; private set; }

	global static String accountType;
	global static String contactType;

	global static String lastLoginDate          { get; private set; }
	global static Integer loginCount            { get; private set; }
	global static String portalStagesMap        { get; private set; }
	global static String cpUser                 { get; private set; }
	global static User cp_User                  { get; private set; }
	global static String cpUserProfilePicUrl    { get; private set; }
	global String pkgNamespace  {get; set;}
	global static Integer gaunletTimeoutParam   { get; private set; }
	global static String expiredTCECError       { get; private set; }
	global static String transferEndOfTheDayError { get; private set; }
	global static String mxInstitutionGuid      { get; private set; }
	global static String accountTypePicklist    { get; private set; }
	global static String portalSupportPhone     { get; private set; }
	global static String portalSupportHours     { get; private set; }
	global String wireInstructionDownloadLink   { get; set;}

	global static Boolean isModuleSMSEnabled    { get; private set; }
	global static Boolean isModuleBillpayEnabled      { get; private set; }
	global static Boolean isBillPayForPayrailzEnabled      { get; private set; }
	global static String payrailzURL     { get; private set; }
	global static String payrailzIframeURL     { get; private set; }
	global static String payrailzFspId     { get; private set; }
	global static String payrailzAuthorization {get; private set;}
	global static Boolean isModuleBusinessAccountsEnabled      { get; private set; }
	global static Boolean isModuleLoanPaymentsEnabled { get; private set; }
	global static Boolean isVerishowEnabled { get; private set; }
	global static Boolean isPlaidEnabled { get; private set; }
	global static String showSavFundCD { get; private set; }
	global static String verishowClientCode { get; private set; }
	global static String plaidEnvironment    { get; private set; }
	global static String plaidAccountKey    { get; private set; }
	global static String portalAdvertVideo { get; private set; }
	global static String portalAdvertHdrL1 { get; private set; }
	global static String portalAdvertHdrL2 { get; private set; }
	global static String portalAdvertHdrL3 { get; private set; }
	global static String portalAdvertActionText { get; private set; }
	global static String portalAdvertHideMainImg { get; private set; }
	global static String portalAdvertHideLowerImg { get; private set; }
	global static String maximumLoanPaymentPercentage { get; private set; }
	global static String getInfoFromLoansEnable {get; private set;}
	global static String beneficialOwnersEnabled { get; private set; }

	global PortalController(){

	}

	global PortalController(nForce.LifeCycleAppController controller) {

	}

	global void initialize(){
		dt = DateTime.now();
		UserSessionID = UserInfo.getSessionId();
		ThreatMetrixSessionId = UserSessionID + dt.getTime();
		OrgId = LiveOak.PortalSetting.get.value(Label.LiveOak.Portal_Setting_Treat_Metrix_Org_Id);

		this.pkgNamespace = LiveOak.APITools.getInstance().namespacePrefix;

		String whereClause;
		String accountId = ApexPages.currentPage().getParameters().get('accountId');
		if (accountId == null) {
			whereClause = 'WHERE Id = \'' + UserInfo.getUserId() + '\'';
		} else {
			whereClause = 'WHERE AccountId = :accountId LIMIT 1';
		}
		User u = Database.query(
			'SELECT Id, Name, Email, Phone, AccountId, ContactId, ProfileId, Profile.Name'+
			' FROM User '+
			whereClause
			);

		init(u);

	}

	global void init(User u){
		cp_User = u;
		this.accountId = cp_User.AccountId;
		cpUser = JSON.serialize(u);

		// If no image was found, look up default image
		if (String.isEmpty(cpUserProfilePicUrl)) {
			List<StaticResource> resourceList= [SELECT Name, NamespacePrefix, SystemModStamp FROM StaticResource WHERE Name = 'CP_Resources'];
			// Checking if the result is returned or not
			if(resourceList.size() == 1) {
				// Getting namespace
				String namespace = resourceList[0].NamespacePrefix;
				// Resource URL
				cpUserProfilePicUrl = '/resource/' + resourceList[0].SystemModStamp.getTime() + '/' + (namespace != null && namespace != '' ? namespace + '__' : '') + 'CP_Resources_img/img/default.jpg';
			} else {
				cpUserProfilePicUrl = '';
			}
		}

		Contact c = [SELECT LLC_BI__Type__c, Account.Type FROM Contact WHERE Id = :u.ContactId];

		contactType = c.LLC_BI__Type__c;
		accountType = c.Account.Type;

		Map<String,String> menuItems = new Map<String,String> {
			'Referral Source' => 'showReferrals',
			'Advisor' => 'showBenchmarks'
		};

		String showItem = menuItems.get(contactType);
		showDashboard = showItem == null ? true : showItem.contains('showDashboard');
		showLoanApplication = showItem == null ? true : showItem.contains('showLoanApplication');
		showReferrals = showItem == null ? false : showItem.contains('showReferrals');
		showBenchmarks = showItem == null ? false : showItem.contains('showBenchmarks');

		// Used to see if this is the first time a user has logged in.
		List<LoginHistory> loginHistoryList = [
			SELECT
			UserId, LoginTime, Status
			FROM
			LoginHistory
			WHERE
			UserId = :u.Id
			         ORDER BY
			         LoginTime DESC
		];
		loginCount = 0;
		for(LoginHistory login : loginHistoryList) {
			if(login.Status == Label.LiveOak.Success) {
				loginCount++;
			}
		}

	}

	@RemoteAction
	global static void forgotPasswordRemote() {

		User user = [SELECT Id, username FROM User WHERE Id =: UserInfo.getUserId()];
		Boolean success = Site.forgotPassword(user.username);

	}
}
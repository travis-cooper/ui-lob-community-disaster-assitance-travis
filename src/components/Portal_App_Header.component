<apex:component id="portal_header">

	<apex:attribute name="guid" type="String" description="global application GUID" />

	<apex:includeScript value="{!$Resource.portal_header_app}" />
	<apex:includeScript value="{!$Resource.portal_header_viewModel}" />

	<style type="text/css">
		.customSelect .select-styled:after {
			background-image: url('{!URLFOR($Resource.chevron_down)}');
		}

		#main-menu {
			position: fixed;
			z-index: 99;
			width: 100%;
		}

		#main-menu .nav-wrapper {
			display: flex;
			width: 100%;
			height: 55px;
			background-color: #fff;
			border-bottom: solid 1px #e6e6e6;
		}

		#main-menu .nav-wrapper .navSection {
			padding-left: 15px;
			border-left: solid 1px #e6e6e6;
			padding-top: 3px;
			width: 100%;
		}

		#main-menu .nav-wrapper .rightNavSection {
			border-left: solid 1px #e6e6e6;
		}

		#main-menu .nav-wrapper .fa-sign-out:before {
			padding-right: 20px;
		}

		#main-menu .nav-wrapper .rightNavSection ul.dropdown {
			margin-top: 9px;
			left: -142px;
		}

		.mobilenavtoggle {
			list-style: none;
		}

		#main-menu .nav-wrapper ul li a {
			font-weight: 300;
			font-size: 15px;
			padding: 0px;
			letter-spacing: .4px;
			font-family: sans-serif;
		}

		#main-menu .nav-wrapper a i {
			margin-left: 5px;
		}

		#main-menu .nav-wrapper i {
			font-size: 16px;
		}

		#main-menu .nav-wrapper .dropdown {
			border-left: solid 1px #e6e6e6;
			border-right: solid 1px #e6e6e6;
			border-bottom: solid 1px #e6e6e6;
			box-shadow: 3px 3px 5px #cccccc;
			padding: 5px 0;
		}

		#main-menu .nav-wrapper .dropdown li a:hover {
			font-weight: 400;
			color: #fff;
			background-color: #a51e31;
			width: 209px;
		}

		#main-menu .nav-wrapper .dropdown li a {
			font-size: 14px;
			width: 200px;
			line-height: 2.5rem;
			padding-left: 10px;
		}

		#main-menu .nav-wrapper .square>i {
			display: -webkit-flex;
			display: flex;
			width: 100%;
			justify-content: center;
			-webkit-align-items: center;
			align-items: center;
			margin: 0;
			height: 50px;
			padding: 0;
			padding-left: 15px;
			width: 50px;
			font-size: 20px;
			color: #808080;
		}

		#main-menu .nav-wrapper .square>i i {
			margin-left: 5px;
		}

		#main-menu .nav-wrapper li.branding {
			background-image: url('{!URLFOR($Resource.Portal_Images, 'portal_images/navLogoFull.png')}');
			background-repeat: no-repeat;
			background-position: center;
			flex: 2;
			min-width: 230px;
			background-size: 80% auto;
			background-color: #fff;
			padding-top: 53px;
		}

		#main-menu .nav-wrapper li.brandingsmall {
			background-image: url('{!URLFOR($Resource.portal_5_assets, 'Portal_5_Assets/img/navLogoTree.png')}');
			background-repeat: no-repeat;
			background-position: center;
			flex: 2;
			width: 100px;
			background-size: 60% auto;
			background-color: #fff;
			padding-top: 52px;
			width
		}

		#main-menu .nav-wrapper .has-dropdown {
			background-color: transparent;
			padding-bottom: 20px;
			margin-right: 15px;
		}

		#main-menu .nav-wrapper button:focus {
			outline: 0;
		}

		#main-menu .nav-wrapper a.active {
			font-weight: 600;
		}

		#main-menu .nav-wrapper .webnav a.subnavactive {
			font-weight: 400;
			color: #fff;
			background-color: #a51e31;
			width: 209px;
		}

		#main-menu .nav-wrapper .rightNavSection a.subnavactive {
			font-weight: 400;
			color: #fff;
			background-color: #a51e31;
			width: 209px;
		}

		#main-menu .nav-wrapper .webnav a.active:after {
			font-weight: 600;
			content: "";
			height: 4px;
			width: 100%;
			top: 50px;
			background-color: #a51e31;
			border: none;
			position: absolute;
			bottom: 0;
			left: 0;
		}

		#main-menu .nav-wrapper a:after {
			border: none;
		}

		#main-menu .nav-wrapper .no-dropdown a.active:after {
			font-weight: 600;
			top: 47px;
		}

		#main-menu .nav-wrapper .no-dropdown {
			position: relative;
			margin-left: 15px;
			margin-right: 15px;
		}

		#main-menu .nav-wrapper .has-dropdown>a.square {
			padding: 0 0;
		}

		#main-menu .nav-wrapper .signout {
			width: 110px;
			padding-left: 15px;
			height: 50px;
			padding-top: 3px;
		}

		#main-menu .nav-wrapper .signout .square {
			width: 50px;
			padding-left: 10px;
		}

		#main-menu .nav-wrapper .has-dropdown.hover>.dropdown,
		.nav-wrapper .has-dropdown.not-click:hover>.dropdown {
			width: 210px;
			margin-top: 11px;
		}

		#main-menu .nav-wrapper .has-dropdown.hover>.dropdown,
		.nav-wrapper .has-dropdown.not-click>.dropdown {
			width: 210px;
			margin-top: 11px;
		}

		#main-menu .nav-wrapper ul li>a {
			background: transparent;
		}

		#main-menu .nav-wrapper .has-dropdown>a {
			padding-right: 0 !important;
		}

		#main-menu .nav-wrapper .has-dropdown>a:after {
			border: none;
		}

		/*MOBILE WEB*/

		#main-menu .nav-wrapper ul.mobileNav {
			display: -webkit-flex;
			display: flex;
			width: 100%;
			-webkit-flex-direction: column;
			-ms-flex-direction: column;
			flex-direction: column;
			list-style: none;
			list-style-type: none;
			margin: 0;
			-webkit-margin-before: 0;
			-webkit-margin-after: 0;
			-webkit-margin-start: 0;
			-webkit-margin-end: 0;
			-webkit-padding-start: 0;
			flex: 1;
			justify-content: flex-end;
			align-items: flex-start;
			float: none;
		}

		#main-menu .nav-wrapper ul {
			list-style: none;
		}

		#main-menu .nav-wrapper ul.mobileNav li {
			float: none;
			width: 100%;
			padding: 5px;
			border-bottom: solid 1px #e6e6e6;
			z-index: 99
		}

		#main-menu .nav-wrapper ul.mobileNav li a {
			padding-left: 10px;
		}

		#main-menu .nav-wrapper ul.mobileNav li a.active {
			font-weight: 600px;
		}

		#main-menu .nav-wrapper .mobilenavtoggle button {
			padding: 0px;
			width: 55px;
			height: auto
		}

		#main-menu .nav-wrapper .mobilenavtoggle button i {
			padding-left: 10px;
		}

		#main-menu .nav-wrapper li button {
			background-color: #fff;
		}

		#main-menu .hidden {
			display: none !important;
		}

		/*OVERRIDE FOUNDATION TOP-BAR STYLES*/

		.top-bar-section ul,
		.top-bar-section ul li,
		.top-bar-section ul li>a.button,
		.top-bar-section ul li>a.button:hover,
		.top-bar-section ul li>a.button.secondary:hover,
		.top-bar-section ul li>a.button.secondary,
		.top-bar-section ul li>a.button.success,
		.top-bar-section ul li>a.button.success:hover,
		.top-bar-section ul li>a.button.alert,
		.top-bar-section ul li>a.button.alert:hover,
		.top-bar-section ul li:hover:not(.has-form)>a,
		.top-bar-section ul li:hover>a,
		.top-bar-section ul li.active>a,
		.top-bar-section .has-dropdown>a:after,
		.top-bar-section .has-dropdown.moved>.dropdown,
		.top-bar-section li.hover>a:not(.button),
		.top-bar-section .has-dropdown>a,
		.top-bar-section .has-dropdown>a:after,
		.top-bar-section .has-dropdown.moved>.dropdown,
		.top-bar-section .has-dropdown.hover>.dropdown,
		.top-bar-section .has-dropdown.not-click:hover>.dropdown,
		.top-bar-section .has-dropdown .dropdown li.has-dropdown>a:after,
		.top-bar-section>ul>.divider,
		.top-bar-section>ul>[role="separator"],
		.top-bar-section li:not(.has-form) a:not(.button),
		.top-bar-section li:not(.has-form) a:not(.button):hover,
		.top-bar-section .dropdown li:not(.has-form):not(.active)>a:not(.button),
		.top-bar-section .dropdown li:not(.has-form):not(.active)>a:not(.button):hover,
		.top-bar-section .dropdown li:not(.has-form):not(.active):hover>a:not(.button),
		.top-bar-section li.active:not(.has-form) a:not(.button),
		.top-bar-section li.active:not(.has-form) a:not(.button):hover,
		.top-bar-section li a:not(.button):hover {
			background: #ffffff;
			color: #474747;
		}
	</style>

	<!-- F5 -->
	<!-- <header> -->
	<div data-bind="with: Portal_Header{!guid}, routeVisible:'default', alwaysVisible: true" style="display: none;">
		<div id="main-menu">
			<!-- WEB 1025px+ -->
			<section class="show-for-large-up">
				<div class="nav-wrapper top-bar-section">
					<ul>
						<li class="branding logo"></li>
					</ul>
					<ul class="navSection webnav">
						<li class="no-dropdown">
							<a navattr="contact-us" href="#{!$Label.Portal_Route_ContactUs}">Contact Us</a>
						</li>
					</ul>
					<ul class="rightNavSection">
						<li class="has-dropdown not-click">
							<a class="square">
								<i class="fa fa-cog" aria-hidden="true">
									<i class="fa fa-caret-down" aria-hidden="true"></i>
								</i>
							</a>
							<ul class="dropdown dropdownParent">
								<li>
									<a class="rightsubnav" navattr="frequently-asked-questions" href="#{!$Label.Portal_Route_Knowledge_Base}">FAQ</a>
								</li>
							</ul>
						</li>
					</ul>
					<ul class="rightNavSection">
						<li class="signout">
							<a href="{!$Site.Prefix}/secur/logout.jsp" alt="resources.html">
								<span>Logout</span>
								<i class="fa fa-sign-out" aria-hidden="true"></i>
							</a>
						</li>
					</ul>
				</div>
			</section>
			<!-- END WEB 1025px+ -->
			<!-- MOBILE -1025px -->
			<section class="nav-wrapper top-bar-section hide-for-large-up">
				<ul style="list-style:none;" class="show-for-medium-only">
					<li class="branding logo"></li>
				</ul>
				<!-- SMALL -641px -->
				<ul class="show-for-small-only">
					<li class="brandingsmall left logo"></li>
				</ul>
				<!-- END SMALL -641px -->
				<li class="mobilenavtoggle">
					<button class="square button right">
						<i class="fa fa-bars" id="mobilenavlogo"></i>
					</button>
				</li>
			</section>
			<section class="nav-wrapper top-bar-section hidden hide-for-large-up" id="subLargeNavList">
				<div class="bottom">
					<ul class="mobileNav">
						<li>
							<a class="mobileli" mobilenavattr="contact-us" href="#{!$Label.Portal_Route_ContactUs}">Contact Us</a>
						</li>
						<li>
							<a class="mobileli" mobilenavattr="frequently-asked-questions" href="#{!$Label.Portal_Route_Knowledge_Base}">FAQ</a>
						</li>
						<li>
							<a class="mobileli" href="{!$Site.Prefix}/secur/logout.jsp" alt="resources.html">Logout</a>
						</li>
					</ul>
				</div>
			</section>
			<!-- END MOBILE -1025px -->
		</div>
		<!-- </header> -->
		<script type="text/javascript">
			(function (app) {

				app.register('Portal_Header{!guid}', {});

			})(new LifeCycle.Portal_Header.App());
		</script>
	</div>
</apex:component>